import React from 'react';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';


const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            flexGrow: 1,
        },
        styleBar: {
            height: '12vh',
            boxShadow: 'none'
        },
        menuButton: {
            marginRight: theme.spacing(2),
        },
        title: {
            flexGrow: 1,
            justifyContent: 'center'
        },
        toolBar: {
            height: '12vh',
            background: '#a9b7c0',
            '@media (min-width: 600px)': {
                minHeight: "12vh"
            }
        },
    }),
);

export default function LogoPlaceholder() {
    const classes = useStyles();

    return (
        <div className={classes.root}>
            <AppBar position="fixed" className={classes.styleBar}>
                <Toolbar className={classes.toolBar}>
                    <Typography variant="h6" className={classes.title}>
                        Linked-LIA
                    </Typography>
                </Toolbar>
            </AppBar>
        </div>
    );
}
