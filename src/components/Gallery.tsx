import React from 'react';
import {Theme, createStyles, makeStyles} from '@material-ui/core/styles';
import ImageList from '@material-ui/core/ImageList';
import ImageListItem from '@material-ui/core/ImageListItem';
import OrionLondonlogo from '../assets/images/OrionLondonlogo.png';
import clipart1094777 from '../assets/images/clipart1094777.png';
import {Box, CardActionArea, ClickAwayListener, Grid} from "@material-ui/core";

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            display: 'flex',
            flexWrap: 'wrap',
        },
        imageList: {
            flexWrap: 'nowrap',
            transform: 'translateZ(6)',

            "&::-webkit-scrollbar": {
                width: 7,
            },
            "&::-webkit-scrollbar-track": {
                boxShadow: `inset 0 0 9px #c7d8c6`,
            },
            "&::-webkit-scrollbar-thumb": {
                backgroundColor: "#c7d8c6",
            },
        },
        position: {
            justifyContent: 'center',
            [theme.breakpoints.up('sm')]: {
                justifyContent: 'flex-end',
            }
        }
    }),
);


const itemData = [
    {
        img: OrionLondonlogo,
        title: '1'
    },
    {
        img: clipart1094777,
        title: '2'
    },
    {
        img: OrionLondonlogo,
        title: '3'
    },
    {
        img: clipart1094777,
        title: '4'
    },
];

export default function Gallery() {
    const classes = useStyles();

    return (
        <Grid container className={classes.position}>
            <Grid item className={classes.root} xs={12} sm={6} lg={5}>
                <ImageList className={classes.imageList} cols={2}>
                    {itemData.map((item) => (
                        <ImageListItem key={item.img}>
                            <img
                                onClick={() => {
                                    alert('Öppna bilden! ' + item.title);
                                }}
                                src={item.img}
                                alt={'Image'}
                            />
                        </ImageListItem>
                    ))}
                </ImageList>
            </Grid>
        </Grid>
    );
}
