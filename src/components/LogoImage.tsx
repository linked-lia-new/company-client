import React from 'react';
import {makeStyles, createStyles, Theme} from '@material-ui/core/styles';
import Avatar from '@material-ui/core/Avatar';
import Spotify from '../assets/images/Spotify.png';
import {Grid} from "@material-ui/core";
import {Link} from "react-router-dom";

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            display: 'grid',
            '& > *': {
                margin: theme.spacing(3),
            },

        },
        img: {
            width: theme.spacing(18),
            height: theme.spacing(18),
            border: '0.1px solid lightgray'
        },
        position: {
            justifyContent: 'center',
            [theme.breakpoints.up('sm')]: {
                justifyContent: 'flex-start',
            },
        }
    }),
);


export default function LogoImage() {
    const classes = useStyles();

    return (
        <Grid container className={classes.position}>
            <Grid item lg={1} md={1} sm={1}/>
                <Grid item className={classes.root}>
                    <Avatar className={classes.img}
                            src={Spotify}
                            alt="Logo"/>
                </Grid>
        </Grid>
    );
}
