import React from 'react';
import {Route, Switch} from 'react-router-dom';
import Profile from '../screens/Profile';
/*import StudentMainScreen from "../screens/StudentMainScreen";
import CompanyProfile from "../screens/CompanyProfile";
import SearchLia from "../screens/SeachLia";
import SearchCompany from "../screens/SearchCompany";
import SavedLia from "../screens/SavedLia";
import LiaAddView from "../screens/LiaAddView";*/

function Routs() {
    return (
        <main>
            <Switch>
                <Route path="/profile" component={Profile} exact/>
                <Route path="/matches"/>
                <Route path="/messages"/>
                <Route path="/visitors"/>
                <Route path="/help"/>
{/*                <Route path="/profile" component={StudentMainScreen}/>
                <Route path="/searchLia" component={SearchLia}/>
                <Route path="/companyProfile" component={CompanyProfile}/>
                <Route path="/searchCompany" component={SearchCompany}/>
                <Route path="/savedLia" component={SavedLia}/>
                <Route path="/appliedLia" component={SearchLia}/>
                <Route path="/liaAdd" component={LiaAddView}/>*/}
                <Route path="/tipsAndTricks"/>
                <Route path="/editLayout"/>
                <Route path="/contactAdmin"/>
                <Route path="/deleteAccount"/>
                <Route path="/welcomePage"/>
            </Switch>
        </main>
    )
}

// <Route path="/profile" component={StudentMainScreen user=student} exact/>

export default Routs;
