import React from 'react';
import {createStyles, makeStyles, Theme} from '@material-ui/core/styles';
import ButtonMenuCompProfile from "../components/ButtonMenuCompProfile";
import {Grid} from "@material-ui/core";
import BigImage from "../components/BigImage";
import Gallery from "../components/Gallery";
import LogoImage from "../components/LogoImage";
import TextArea from "../components/TextArea";

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            //stylings :)
        }
    }),
);

export default function CompanyProfile() {
    const classes = useStyles();

    return (
        <Grid container className={classes.root}>
            <BigImage/>
            <LogoImage/>
            <ButtonMenuCompProfile/>
            <TextArea/>
            <Gallery/>
        </Grid>
    );
}
